
/*  Ruta para ejecutar el pago, 
    necesita dos parametros en el get: paymentId y PayerID, 
    devuelve los datos de la transaccion. 
    /api/paypal/store?paymentId=kkjkk&PayerID=yfufufuf

 En model Paypal de deben setear la rutas de redireccion que usara paypal
*/
Route::get('paypal/store', 'PaypalController@store');

/*  Ruta para generar un pago
    Devuelve un link, al cual se debe redirigir al usuario
*/
Route::get('paypal', 'PaypalController@index');


/** NOTA
 * Para que funcione se debe modificar el sdk de paypal.
 * 
 * el archivo vendor/paypal/rest-api-sdk-php/lib/PayPal/Common/PayPalMode.php
 *  la linea 179 debe ser:  elseif (is_array($v) && sizeof($v) <= 0)
 */