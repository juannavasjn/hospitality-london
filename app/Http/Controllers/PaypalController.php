<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paypal;
use Exception;

class PaypalController extends Controller
{
    public function index(){

        try{
            $paypal = new Paypal([]);

            $payment =  $paypal->generate();
    
            return response()->json($payment->getApprovalLink());
            
        }catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function store(Request $request){

        try{
            $paypal = new Paypal([]);

            $execute = $paypal->execute($request->paymentId, $request->PayerID)->toArray();
           
            $response = [
                'id' => $execute['id'],
                'intent' => $execute['intent'],
                'state' => $execute['state']
            ];
           
            return response()->json($response, 201);
        }catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ]);
        }
       

    }
}
