<?php

namespace App;

class Paypal
{
    private $_apiContext;
    private $shopping_cart;
    private $_ClientId = 'AWOCdnOXzdI6w9-pfhYU7W23X3JBYYTL4_cIw0XTO8ZXTslQqoV2guIKCmxrxc2xaSzeufCPbAz5HwWi';
    private $_ClientSecret = 'EDPh6YDhj51_J_fy2JNRA31wnY4BZ4VTjr293Mls5gSEbmvivqaDoxl5ildiABeSPxoo9_frc-HSrHoz';

    public function __construct($shopping_cart){

        $this->_apiContext = \PaypalPayment::ApiContext($this->_ClientId, $this->_ClientSecret);

        $config = config("paypal_payment");
        $flatConfig = array_dot($config);

        $this->_apiContext->setConfig($flatConfig);

        $this->shopping_cart = $shopping_cart;
    }

    public function generate(){

        $payment = \PaypalPayment::payment()->setIntent('sale')
                                ->setPayer($this->payer())
                                ->setTransactions([$this->transaction()])
                                ->setRedirectUrls($this->redirectURLs());

        try{
            $payment->create($this->_apiContext);
        }catch(\Exception $e){

            return($e->getMessage());
            //dd($e);
        }

        return $payment;
    }

    public function payer(){
        // Return payment's info

        return \PaypalPayment::payer()
                            ->setPaymentMethod('paypal');
    }

    public function transaction(){
        //Return transaction's info

        return \PaypalPayment::transaction()
                            ->setAmount($this->amount())
                            ->setItemList($this->items())
                            ->setDescription('Booking Hospitality London')
                            ->setInvoiceNumber(uniqid());

    }
    public function paypalItem(){
        return \PaypalPayment::item()->setName('Reservation')
                                ->setDescription('Reservation Hospitality London')
                                ->setCurrency('GBP')
                                ->setQuantity(1)
                                ->setPrice('35');
    }
    public function items(){

        $items = array();

        array_push($items, $this->paypalItem());

        return \PaypalPayment::itemList()->setItems($items);
    }

    public function amount(){
        
        return \PaypalPayment::amount()->setCurrency("GBP")
                            ->setTotal('35'); // Aqui se podria convertir de la moneda local a USD
    }

    public function redirectUrls(){

        $baseUrl = url('/');
        return \PaypalPayment::redirectUrls()
                            ->setReturnUrl("$baseUrl")
                            ->setCancelUrl("$baseUrl");
    }

    public function execute($paymentId, $payerId){
        $payment = \PaypalPayment::getById($paymentId, $this->_apiContext);

        $execution = \PaypalPayment::PaymentExecution()
                                    ->setPayerId($payerId);

        return $payment->execute($execution, $this->_apiContext);
    }
}